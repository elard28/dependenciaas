package dependencias;


/**
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */

public class Impresora
{
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 */
	public Impresora(){
		
	}

	public void formato(Formato form){
		String f = form.getFormato();
        System.out.println(f);
	}	

	public void imprimir(Documento doc){
		String texto = doc.geTexto();
        System.out.println(texto);
	}

}

