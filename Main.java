package dependencias;


/**
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */

public class Main
{
	public static void main(String[] args) {
		Documento doc=new Documento("hola");
		Impresora imp=new Impresora();
		Formato fr=new Formato("A4");
		Oficina of=new Oficina("grande");
		Persona pr=new Persona("Sandalio");
		Archivos ar=new Archivos();

		of.imprimir_copias(2,imp,doc,fr);
		pr.asignar(of);
		ar.indicar_pertenencia(pr);

	}

}