package dependencias;


/**
 * <!-- begin-user-doc -->
 * <!--  end-user-doc  -->
 * @generated
 */

public class Oficina
{
	/**
	 * <!-- begin-user-doc -->
	 * <!--  end-user-doc  -->
	 * @generated
	 */

	public String tipo;

	public Oficina(String t){
		tipo=t;
	}

	public void imprimir_copias(int veces, Impresora imp, Documento doc, Formato form){
		imp.formato(form);
		while(veces>0){
			imp.imprimir(doc);
			veces=veces-1;
		}
	}

}

